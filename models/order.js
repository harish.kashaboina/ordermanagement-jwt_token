const Joi = require('joi');
const SchemaModel = require('../config/schema');

const config = require('../config/config');
// const storeName = config.STORE_NAME;
// const tableName = `${storeName}_orders`; //order table

const tableName = 'rgukt_order_data'

const optionalParams = [
    'title',
    'price'
];
// const optionalParams = [
//     'title',
//     'paymentStatus',
//     'deliveryAddress',
//     'deliveryStatus',
//     'cancellationStatus',
//     'refundStatus',
//     'replacementStatus',
//     'orderDetails',
//     'size',
//     'subFrequency',
//     'subUnit',
//     'requestType',
//     'price'
// ];
const attToGet = ['title', 'order_id', 'price'];
// const attToGet = ['title', 'order_id', 'price','paymentStatus', 'deliveryStatus', 'deliveryAddress'];
// const attToQuery = ['description', 'imageURL', 'price', 'size', 'title', 'collectionId'];
const attToQuery = ['order_id','size', 'title'];

const orderSchema =  {
    hashKey: 'order_id',
    timestamps: true,
    schema: Joi.object({
        order_id: Joi.string().alphanum(),
        price: Joi.number().positive().min(1),
        title: Joi.string()
        
    }).unknown(true).optionalKeys(optionalParams)
};
//unknow(true) will allow us to give parameters not defined in the schema
//Optional keys will allow us to mark those parameters as optional
const optionsObj = {
    attToGet,
    attToQuery,
    optionalParams,
    tableName
};
const Order = SchemaModel(orderSchema, optionsObj);

module.exports = Order;