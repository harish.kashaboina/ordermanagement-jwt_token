const router = require('express').Router();

const Order = require('../../models/order');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');

const apiOperation = function (req, res, crudMethod) {
    const bodyParams = Methods.initializer(req,Order);
    crudMethod(bodyParams, (err, order) => {
        console.log('\ncallback name', crudMethod.name);
        console.log('\norder data\n', order);
        res.send(order);
    });
};

const updateData = function (req, res, updateParams) {
    // const bodyParams = initializer(req, Order);
    
    //if we need any extra params to be updated from the body parameters. we can add them to the 
    //updated parameters obejct and pass it
    Order.updateItem(updateParams, (err, order) => {
        console.log('\nUpdated order data\n', order);
        res.send(order);
    });
};

router.post('/getorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                    apiOperation(req, res, Order.getItem);
                       
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.post('/addorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    console.log('\ntoken: ', token);
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                // apiOperation(req, res, Order.createItem);
                const bodyParams = Methods.initializer(req, Order);
                console.log('bodyParams: ', bodyParams);
				Order.createItem(bodyParams, {
					table: `${decode.storeName}_order_data`,
					overwrite: true
				}, (err, cart) => {
					console.log('\order data\n', cart);
					res.send(cart);
				})
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.put('/updateorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                    const tabletemp = `${decode.storeName}_order_data`;
                    // const tempParams = Methods.initializer(req, Order);
                    console.log('req.body: ', req.body);
                    Order.selectTable(tabletemp);
                    Order.updateItem( req.body,{},updateCallback);

                    function updateCallback(err, orderData) {
                        console.log('\nThe updated order data is...\n', orderData);
                        res.send(orderData);
                    }
                }
            })
        }
        else{
            res.send("please send a token");
        }
});

router.put('/cancelorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                    const tabletemp = `${decode.storeName}_order_data`;
                    // const tempParams = Methods.initializer(req, Order);
                    console.log('req.body: ', req.body);
                    Order.selectTable(tabletemp);
                    Order.updateItem({
                        order_id: req.body.order_id,
                        cancellationSatus: true
                    }, {},(err, order) => {
                        console.log('\nUpdated order data\n', order);
                        res.send(order);
                    });
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.put('/replaceorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                    const tabletemp = `${decode.storeName}_order_data`;
                    // const tempParams = Methods.initializer(req, Order);
                    console.log('req.body: ', req.body);
                    Order.selectTable(tabletemp);
                    Order.updateItem({
                        order_id: req.body.order_id,
                        replacementStatus: true
                    }, {},(err, order) => {
                        console.log('\nUpdated order data\n', order);
                        res.send(order);
                    });
                }
            })
        }
        else{
            res.send("please send a token");
        }
});

router.put('/returnorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                    const tabletemp = `${decode.storeName}_order_data`
                    //Passing and selecting the table to operate on dynamically
                    
                    Order.selectTable(tabletemp)
                    //passing the conditional object here {} as second parameter
                    Order.updateItem({
                        order_id: req.body.order_id,
                        returnStatus: true
                    }, {},(err, order) => {
                        console.log('\nUpdated order data\n', order);
                        res.send(order);
                    });
                
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.delete('/deleteorder', (req, res) => {
    var token = req.body.token || req.headers['token'];
    
        if(token){
            jwt.verify(token, config.SECRET_KEY, function(err, decode){
                if(err){
                    res.status(500).send("Invalid Token");
                }
                else{
                // const removeCallback = (err, removeData) => {
                //     console.log('\nThe removed item data is...\n', removeData);
                //     res.send(removeData);
                // }
                // Order.deleteItem({
                //     order_id: req.body.order_id
                // }, removeCallback);
                const tabletemp = `${decode.storeName}_order_data`
                //Passing and selecting the table to operate on dynamically
                
                Order.selectTable(tabletemp)
                //passing the conditional object here {} as second parameter
                Order.deleteItem({
                    order_id: req.body.order_id }, {},(err, order) => {
                        if(err){
                            res.send({status:"failure", error: err});
                        }
                    console.log('\nUpdated order data\n', order);
                    res.send({status:"success", msg: "deleted"});
                });    
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

module.exports = router;